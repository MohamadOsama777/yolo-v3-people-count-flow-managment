# Python Traffic Counter

The code on this prototype uses the code structure developed by Adrian Rosebrock for his article [YOLO object detection with OpenCV](https://www.pyimagesearch.com/2018/11/12/yolo-object-detection-with-opencv).

## Quick Start

1. Download the code to your computer.
```
$ git clone https://bitbucket.org/MohamadOsama777/yolo-v3-people-count-flow-managment.git
```
2. [Download yolov3.weights](https://www.dropbox.com/s/99mm7olr1ohtjbq/yolov3.weights?dl=0) and place it in `/yolo-coco`.
```
$ cd yolo-v3-people-count-flow-managment/yolo-coco/
$ wget https://pjreddie.com/media/files/yolov3.weights
```
3. Make sure you have Python 3.7.0 and [OpenCV 3.4.2](https://www.pyimagesearch.com/opencv-tutorials-resources-guides/) installed.
```
$ apt update
$ apt install python3-pip python3-numpy
$ pip3 install imutils opencv-contrib-python==4.1.1.26 numba sklearn filterpy
$ apt install python-opencv
```
4. Run:
```
$ python3 main.py --input input/your-video.mp4 --output output/any-output.avi --yolo yolo-coco
```

### Thanks